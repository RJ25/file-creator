const fs = require('fs');
const colors = require('colors');
const randomstring = require('randomstring');
const prompt = require('prompt');
var filecontent = fs.readFileSync('./content/content.txt')
let create = (amount,path) => {
  for(i=0;i<parseInt(amount);i++){
    var name = randomstring.generate(4);
    fs.appendFileSync(`${path}${name}.txt`,`${filecontent}`)
  }
}
let run = async () => {
  try{
    prompt.start();
    let { amount, path } = await prompt.get(['amount','path'])
    if(path === undefined) path = './';
    if(path.toString().endsWith('/') === false) path = [`${path}`,'/'].join('')
    if(fs.existsSync(path) === false){
      throw new SyntaxError('non-existent path')
    }
    create(amount,path)
  }catch(err){
    console.log('Can\'t run. If you are using a path, please end path with \'/\'.'.brightRed.bold)
    console.error(err)
  }

}
run()